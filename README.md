# Utilisation des services sur Kubernetes



## Liste des fichiers
Vous retrouverez sur ce dépôt les trois fichiers YAML permettant de déclarer des services de type ClusterIP, LoadBalancer et NodePort :
- service-clusterip.yaml
- service-loadbalancer.yaml
- service-nodeport.yaml